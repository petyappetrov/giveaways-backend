/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import { V1 as Client } from 'instagram-private-api'
import jwt from 'jsonwebtoken'
import { ForbiddenError, UserInputError, AuthenticationError } from 'apollo-server'
import config from '../config'

const User = mongoose.model('User')

class Instagram {
  async login ({ name, password }) {
    this.device = new Client.Device(name)
    this.storage = new Client.CookieMemoryStorage()
    this.session = await Client.Session.create(this.device, this.storage, name, password)
      .catch((error) => {
        if (error.name === 'AuthenticationError' && error.message) {
          if (error.message.includes(
            'The password you entered is incorrect'
          )) {
            throw new UserInputError('Ошибка авторизации', {
              invalidArgs: {
                password: 'Введен неверный пароль.'
              }
            })
          }
          if (error.message.includes(
            'The username you entered doesn\'t appear to belong to an account.'
          )) {
            throw new UserInputError('Ошибка авторизации', {
              invalidArgs: {
                name: 'Аккаунт с таким имененем не существует.'
              }
            })
          }
        }
        throw new AuthenticationError()
      })

    this.account = await this.session.getAccount()

    let user = await User.findOne({ instagram: this.account.params.id })
    if (!user) {
      user = await this.createUser(this.account.params)
    }
    user.jwt = jwt.sign({ _id: user._id, role: user.role }, config.jwtSecret)
    return user
  }

  async createUser ({ fullName, username, biography, profilePicUrl, id }) {
    const user = await User.create({
      name: fullName,
      email: username + '@instagram.com',
      about: biography,
      photoUrl: profilePicUrl,
      instagram: id,
      role: 'client'
    })
    if (!user) {
      throw new ForbiddenError()
    }
    return user
  }
}

export default new Instagram()
