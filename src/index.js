/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

/**
 * Babel hook for enable to es2015 features
 */
require('babel-core/register')

/**
 * Connecting to DB
 */
require('./database')
