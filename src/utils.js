/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import cookie from 'cookie'
import { get, split } from 'lodash'

const resolveAuthorizationHeader = (req) => {
  const authorization = get(req, 'headers.authorization')
  if (!authorization) {
    return null
  }
  const parts = split(authorization, ' ')
  if (parts.length === 2) {
    if (/^Bearer$/i.test(parts[0])) {
      return parts[1]
    }
  }
}

const resolveCookie = (req) => {
  const cookies = get(req, 'headers.cookies') || get(req, 'cookies')
  if (!cookies) {
    return null
  }
  const parsedCookies = cookie.parse(cookies)
  const token = parsedCookies._gajwt
  return token
}

export const getToken = (req) => {
  const token = resolveAuthorizationHeader(req) || resolveCookie(req)
  return token
}
