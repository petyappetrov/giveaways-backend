/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const Joi = require('joi')

/**
 * Parse .env file and include in process.env
 */
require('dotenv').config()

/**
 * Schema for env variables
 */
const envSchema = Joi.object({
  NODE_ENV: Joi.string().required(),
  PORT: Joi.number().default(8080),
  DB_HOST: Joi.string().required(),
  DB_NAME: Joi.string().required(),
  JWT_SECRET: Joi.string().required()
})

/**
 * Validate env variables
 */
const { error, value } = Joi.validate(process.env, envSchema.unknown())
if (error) {
  console.error(error.message)
}

/**
 * Project configuration variables
 */
export default {
  env: value.NODE_ENV,
  port: value.PORT,
  dbHost: value.DB_HOST,
  dbName: value.DB_NAME,
  jwtSecret: value.JWT_SECRET
}
