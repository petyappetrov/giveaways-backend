/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { isEmpty } from 'lodash'
import mongoose from 'mongoose'
import moment from 'moment'
import { AuthenticationError, UserInputError, ForbiddenError } from 'apollo-server'
import Instagram from '../../services/instagram'

const User = mongoose.model('User')
const Contest = mongoose.model('Contest')

/**
 * Users resolvers
 */
const UsersResolvers = {
  Query: {
    users: (root, args) => User.getPaginatedList(args),
    user: (root, args) => User.findById(args._id),
    me: async (root, args, context) => {
      if (isEmpty(context.user)) {
        return null
      }
      return User.findById(context.user._id)
    }
  },
  User: {
    createdAt: (root, args) => moment(root.createdAt).format(args.format),
    contests: (root, args) => Contest.getPaginatedList({ ...args, createdBy: root._id })
  },
  Mutation: {
    registerUser: async (root, args) => {
      const user = await User.findOne({ email: args.email })
      if (user) {
        throw new UserInputError('Ошибка регистрации', {
          invalidArgs: {
            email: 'Пользователь с таким e-mail уже зарегистрирован'
          }
        })
      }
      return User.create({ ...args, role: 'client' })
    },
    registerAdmin: () => (root, args, context) => {
      if (context.user && context.user.role !== 'admin') {
        throw new AuthenticationError('Вы не администратор')
      }
      return User.create({ ...args, role: 'admin' })
    },
    loginUser: (root, args) => User.authentication(args),
    loginAdmin: async (root, args) => {
      const user = await User.authentication(args)
      if (user.role !== 'admin') {
        throw new AuthenticationError('Вы не администратор')
      }
      return user
    },
    removeUser: (root, args) => User.findOneAndRemove({ _id: args._id }),
    loginInstagram: (root, args) => Instagram.login(args),
    updateUser: async (root, args, context) => {
      if (!context.user) {
        throw new AuthenticationError()
      }
      const user = await User.findByIdAndUpdate(context.user._id, args, { new: true })
      if (!user) {
        throw new ForbiddenError()
      }
      return user
    }
  }
}

export default UsersResolvers
