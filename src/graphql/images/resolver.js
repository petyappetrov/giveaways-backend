/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

const Image = mongoose.model('Image')

/**
 * Images resolvers
 */
const ImagesResolvers = {
  Mutation: {
    uploadImage: (root, args, context) => {
      // todo
      return Image.create(args)
    },

    removeImage: (root, args) => {
      // todo
      return Image.remove(args._id)
    }
  }
}

export default ImagesResolvers
