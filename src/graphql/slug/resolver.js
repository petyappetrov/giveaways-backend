/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

const User = mongoose.model('User')
const Contest = mongoose.model('Contest')

/**
 * Users resolvers
 */
const SlugResolvers = {
  Query: {
    slug: async (root, args) => {
      const findUser = User.findOne({ slug: args.slug })
      const findContest = Contest.findOne({ slug: args.slug })
      const user = await findUser
      if (user) {
        return user
      }
      const contest = await findContest
      if (contest) {
        return contest
      }
      return null
    }
  },
  Slug: {
    __resolveType: (obj) => obj.name ? 'User' : 'Contest'
  },
  Mutation: {}
}

export default SlugResolvers
