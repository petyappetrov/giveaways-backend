/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'
import { AuthenticationError, ForbiddenError, ApolloError } from 'apollo-server'

const Contest = mongoose.model('Contest')
const Image = mongoose.model('Image')
const User = mongoose.model('User')

/**
 * Contests resolvers
 */
const ContestsResolvers = {
  Query: {
    contests: (root, args) => Contest.getPaginatedList(args),
    contest: (root, args) => Contest.findOne({ slug: args.slug })
  },

  Contest: {
    images: (root) => Image.find({ contestId: root._id }),
    author: (root) => User.findById(root.createdBy),
    createdAt: (root, args) => moment(root.createdAt).format(args.format),
    startDate: (root, args) => moment(root.startDate).format(args.format),
    endDate: (root, args) => moment(root.endDate).format(args.format)
  },

  Mutation: {
    createContest: (root, args, context) => {
      if (!context.user) {
        throw new AuthenticationError()
      }
      return Contest.create({ ...args, createdBy: context.user._id })
    },

    updateContest: async (root, args, context) => {
      const contest = await Contest.findById(args._id)

      if (!contest) {
        throw new ApolloError('Конкурс не найден')
      }

      if (!contest.createdBy.equals(mongoose.Types.ObjectId(context.user._id))) {
        throw new ForbiddenError('Отказано в доступе')
      }

      return contest.update(args)
    },

    removeContest: async (root, args, context) => {
      const contest = await Contest.findById(args._id)

      if (!contest) {
        throw new ApolloError('Конкурс не найден')
      }

      if (
        context.user.role !== 'admin' &&
        !contest.isCreator(context.user._id)
      ) {
        throw new ForbiddenError('Отказано в доступе')
      }

      return contest.remove()
    }
  }
}

export default ContestsResolvers
