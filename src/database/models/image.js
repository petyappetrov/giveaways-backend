/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

/**
 * Image schema
 */
const ImageSchema = new mongoose.Schema({
  url: {
    type: String,
    trim: true,
    required: true
  },
  thumbnail: {
    type: String,
    trim: true,
    required: true
  }
}, {
  versionKey: false
})

mongoose.model('Image', ImageSchema)
