/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import URLSlugs from 'mongoose-url-slugs'
import translit from 'translitit-cyrillic-russian-to-latin'
import jwt from 'jsonwebtoken'
import { forEach } from 'lodash'
import Joi from 'joi'
import { UserInputError } from 'apollo-server'
import config from '../../config'

/**
 * User schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    trim: true,
    type: String,
    required: true,
    unique: true,
    validate: [
      (email) => Joi.validate(email, Joi.string().email()),
      'Неверная электронная почта'
    ]
  },
  slug: {
    type: String,
    unique: true,
    required: true
  },
  password: String,
  role: {
    type: String,
    enum: [
      'admin',
      'client'
    ],
    required: true
  },
  photoUrl: String,
  about: String,
  instagram: {
    type: String,
    unique: true,
    sparse: true
  },
  twitter: {
    type: String,
    unique: true,
    sparse: true
  }
}, {
  versionKey: false,
  timestamps: true
})

/**
 * Pre hooks
 */
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    return next()
  }
  try {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.password, salt)
    this.password = hash
    return next()
  } catch (error) {
    return next(error)
  }
})

/**
 * Methods
 */
UserSchema.methods = {
  comparePassword (candidatePassword) {
    return bcrypt.compare(candidatePassword, this.password)
  }
}

/**
 * Static
 */
UserSchema.statics = {
  async authentication ({ email, password }) {
    const user = await this.findOne({ email })

    if (!user) {
      throw new UserInputError('Ошибка авторизации', {
        invalidArgs: {
          email: 'Пользователь с таким email адресом не зарегистрирован'
        }
      })
    }

    const isMatch = await user.comparePassword(password)

    if (!isMatch) {
      throw new UserInputError('Ошибка авторизации', {
        invalidArgs: {
          password: 'Неверный пароль'
        }
      })
    }

    user.jwt = jwt.sign({ _id: user._id, role: user.role }, config.jwtSecret)

    return user
  },

  getPaginatedList ({ filters, sortField, sortOrder, limit, skip, search }) {
    const match = {}

    /**
     * Search by name
     */
    if (search) {
      match.name = {
        $regex: search,
        $options: 'i'
      }
    }

    /**
     * Filters
     */
    if (filters) {
      forEach(filters, (filters, field) => {
        if (filters.length) {
          match[field] = {
            $in: filters
          }
        }
      })
    }

    /**
     * Stages for aggregation
     */
    const stages = [
      {
        $match: match
      }
    ]

    /**
     * Sorting by fields
     */
    if (sortField && sortOrder) {
      stages.push({
        $sort: {
          [sortField]: sortOrder === 'ascend' ? 1 : -1
        }
      })
    }

    /**
     * By pagination
     */
    if (skip) {
      stages.push({
        $skip: skip
      })
    }
    if (limit) {
      stages.push({
        $limit: limit
      })
    }

    return {
      items: this.aggregate(stages),
      count: this.countDocuments()
    }
  }
}

/**
 * Plugins
 */
UserSchema.plugin(URLSlugs('email', {
  indexUnique: true,
  generator: (text) => translit(text).trim().toLowerCase().replace(/([^a-z0-9\-\\_]+)/g, '-')
}))

/**
 * Create model
 */
mongoose.model('User', UserSchema)
