/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import URLSlugs from 'mongoose-url-slugs'
import translit from 'translitit-cyrillic-russian-to-latin'
import { forEach, get } from 'lodash'

/**
 * Contest schema
 */
const ContestSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  slug: {
    type: String,
    unique: true,
    required: true
  },
  socialNetwork: {
    type: String,
    enum: [
      'twitter',
      'instagram'
    ],
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  thumbnailUrl: {
    type: String,
    required: true
  },
  type: {
    type: String,
    enum: [
      'random',
      'like',
      'repost',
      'comment'
    ],
    required: true
  },
  description: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  hashtag: {
    type: String,
    required: true
  },
  countWinners: {
    type: Number,
    required: true,
    min: 1,
    max: 10
  },
  startDate: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }
})

/**
 * Methods
 */
ContestSchema.methods = {
  isCreator (_id) {
    return !this.createdBy.equals(mongoose.Types.ObjectId(_id))
  }
}

/**
 * Statics
 */
ContestSchema.statics = {
  async getPaginatedList ({
    sortField = 'createdAt',
    sortOrder = 'descend',
    limit = 20,
    skip = 0,
    filters,
    search,
    createdBy,
    status
  }) {
    const match = {}

    /**
     * Filter active or inactive statuses
     */
    if (status) {
      const statuses = {
        active: {
          $gte: new Date()
        },
        inactive: {
          $lte: new Date()
        }
      }
      match.endDate = statuses[status]
    }

    /**
     * Search by title
     */
    if (search) {
      match.title = {
        $regex: search,
        $options: 'i'
      }
    }

    if (createdBy) {
      match.createdBy = mongoose.Types.ObjectId(createdBy)
    }

    /**
     * Filters
     */
    if (filters) {
      forEach(filters, (filters, field) => {
        match[field] = {
          $in: filters
        }
      })
    }

    const results = await this.aggregate([
      {
        $match: match
      },
      {
        $sort: {
          [sortField]: sortOrder === 'ascend' ? 1 : -1
        }
      },
      {
        $group: {
          _id: null,
          count: {
            $sum: 1
          },
          items: {
            $push: '$$ROOT'
          }
        }
      },
      {
        $project: {
          count: 1,
          items: {
            $slice: ['$items', skip, limit]
          }
        }
      }
    ])

    return get(results, '0', { items: [], count: 0 })
  }
}

/**
 * Plugins
 */
ContestSchema.plugin(URLSlugs('title', {
  indexUnique: true,
  generator: (text) => translit(text).trim().toLowerCase().replace(/([^a-z0-9\-\\_]+)/g, '-')
}))

/**
 * Create model
 */
mongoose.model('Contest', ContestSchema)
