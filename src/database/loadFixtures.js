/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import { keys, map } from 'lodash'
import importDir from 'import-dir'

const fixtures = importDir('./fixtures')

const loadFixtures = () =>
  Promise.all(map(keys(mongoose.models), async (modelName) => {
    const model = mongoose.model(modelName)
    const fixture = fixtures[modelName.toLocaleLowerCase()]
    const item = await model.findOne()
    if (!item) {
      return model.create(fixture)
    }
    return Promise.resolve()
  }))

export default loadFixtures
