/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import importDir from 'import-dir'
import config from '../config'
import loadFixtures from './loadFixtures'

/**
 * Connect to database from mongoose
 */
mongoose.connect(config.dbHost, {
  dbName: config.dbName,
  useNewUrlParser: true
})
mongoose.connection.on('error', console.error)
mongoose.connection.on('open', async () => {
  /**
   * Load mongoose models
   */
  importDir('./models')

  /**
   * Load fixtures from json files
   */
  await loadFixtures()

  /**
   * Run server
   */
  require('../server')
})
