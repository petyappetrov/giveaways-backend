/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { ApolloServer } from 'apollo-server'
import jwt from 'jsonwebtoken'
import { merge } from 'lodash'
import config from './config'
import { getToken } from './utils'

/**
 * Contests
 */
import ContestsSchema from './graphql/Contests/schema.graphql'
import ContestsResolvers from './graphql/Contests/resolver'

/**
 * Users
 */
import UsersSchema from './graphql/users/schema.graphql'
import UsersResolvers from './graphql/users/resolver'

/**
 * Images
 */
import ImagesSchema from './graphql/images/schema.graphql'
import ImagesResolvers from './graphql/images/resolver'

/**
 * Slug
 */
import SlugSchema from './graphql/slug/schema.graphql'
import SlugResolvers from './graphql/slug/resolver'

/**
 * Types schemas
 */
const typeDefs = [
  ContestsSchema,
  UsersSchema,
  ImagesSchema,
  SlugSchema
]

/**
 * Resolvers
 */
const resolvers = merge(
  ContestsResolvers,
  UsersResolvers,
  ImagesResolvers,
  SlugResolvers
)

/**
 * Passing decoded JWT to context
 */
const context = ({ req }) => {
  const token = getToken(req)
  if (!token) {
    return null
  }
  return jwt.verify(token, config.jwtSecret, (error, user) => {
    if (error) {
      return null
    }
    return { user }
  })
}

/**
 * Initialize server
*/
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context
})

/**
 * Start application
 */
server.listen(config.port).then(({ url }) =>
  console.log(`✅  The server is running at ${url}`)
)
